'use strict';

module.exports = iio => {
    const contexts = iio.discover('usb');

    if (contexts.length > 0) {
        console.log('found: Pluto SDR board');
    } else {
        return undefined;
        // throw new Error('please connect Pluto SDR board');
    }

    const pluto0 = contexts[0].devices;

    const dev = {
        tx:  pluto0['cf-ad9361-dds-core-lpc'],
        rx:  pluto0['cf-ad9361-lpc'],
        phy: pluto0['ad9361-phy']
    };

    dev.phy.chan.output.voltage0.attr.hardwaregain.setString(0);

    dev.tx.chan.output.voltage0.attr.sampling_frequency.setString(262500);
    dev.tx.chan.output.voltage1.attr.sampling_frequency.setString(262500);

    dev.tx.chan.output.altvoltage0.attr.sampling_frequency.setString(2090000); // TX1_I_F1
    dev.tx.chan.output.altvoltage1.attr.sampling_frequency.setString(2090000); // TX1_I_F2
    dev.tx.chan.output.altvoltage2.attr.sampling_frequency.setString(2090000); // TX1_Q_F1
    dev.tx.chan.output.altvoltage3.attr.sampling_frequency.setString(2090000); // TX1_Q_F2

    dev.tx.chan.output.altvoltage0.attr.frequency.setString(0); // TX1_I_F1
    dev.tx.chan.output.altvoltage1.attr.frequency.setString(0); // TX1_I_F2
    dev.tx.chan.output.altvoltage2.attr.frequency.setString(0); // TX1_Q_F1
    dev.tx.chan.output.altvoltage3.attr.frequency.setString(0); // TX1_Q_F2

    dev.tx.chan.output.altvoltage0.attr.scale.setString(0.5); // TX1_I_F1
    dev.tx.chan.output.altvoltage1.attr.scale.setString(0.5); // TX1_I_F2
    dev.tx.chan.output.altvoltage2.attr.scale.setString(0.5); // TX1_Q_F1
    dev.tx.chan.output.altvoltage3.attr.scale.setString(0.5); // TX1_Q_F2

    dev.phy.chan.output.voltage2.attr.sampling_frequency.setString(2090000);
    dev.phy.chan.output.voltage3.attr.sampling_frequency.setString(2090000);

    return dev;
};
