'use strict';

const React = require('react');
const ReactDOM = require('react-dom');

const reSvgFull = require('./re-svg-full');
const reBox = require('./re-box');

const $ = React.createElement;
const SvgFull = reSvgFull(React);
const Box = reBox(React);

ReactDOM.render(
    $(SvgFull, {},
        $(Box, {pos: [0, 0, 3, 2], fill: 'hsl(0, 25%, 25%)'}),
        $(Box, {pos: [0, 2, 1, 1], fill: 'hsl(89, 25%, 25%)'}),
        $(Box, {pos: [2, 2, 1, 1], fill: 'hsl(232, 25%, 25%)'})
    ),
    document.getElementById('root')
);

/* eslint-env browser */
