'use strict';

// full screen SVG box

const debounce = require('lodash.debounce');
const t = require('./t');

module.exports = React => {
    const $ = React.createElement;
    return class SvgFull extends React.Component {

        constructor(props) {
            super(props);
            this.state = this.props;
            this.setSize = this.setSize.bind(this);
        }

        setSize () {
            this.setState({sz: [window.innerWidth - 1, window.innerHeight - 1]});
        }

        componentWillMount () {
            window.addEventListener('resize', debounce(this.setSize, 250));
            this.setSize();
        }

        render () {

            const gr = this.props.children.reduce((p, kid) => {
                const pos = kid.props.pos;
                p[0] = Math.max(p[0], pos[0] + pos[2]);
                p[1] = Math.max(p[1], pos[1] + pos[3]);
                return p;
            }, [1, 1]);

            const sz = this.state.sz;

            const step = [
                ((sz[0] / gr[0]) |0),
                ((sz[1] / gr[1]) |0)
            ];

            return $('svg',
                {
                    width: this.state.sz[0],
                    height: this.state.sz[1]
                },
                React.Children.map(this.props.children, e => {
                    // const grid = this.state.grid[i];
                    const grid = e.props.pos;
                    let p1 = t(grid[0] * step[0], grid[1] * step[1]);
                    const p2 = {
                        width: grid[2] * step[0],
                        height: grid[3] * step[1]
                    };
                    return $('g', p1, React.cloneElement(e, p2));
                })
            );
        }
    };
};

/* eslint-env browser */
