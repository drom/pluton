'use strict';

const React = require('react');
const ReactDOM = require('react-dom');

const usb = require('usb');
// const iio = require('libiio');

const reFreq = require('./re-freq');
const rePluto = require('../lib/re-pluto');
const reKeyer = require('../lib/re-keyer');
const reGpio = require('../lib/re-gpio');
const getGpio = require('../lib/get-gpio');

// const keyer = require('./keyer');
// const spectr = require('./spectrogram');

const $ = React.createElement;

const TxFreq = reFreq(React);
const RxFreq = reFreq(React);
const Pluto = rePluto(React);
const Gpio = reGpio(React);
const Keyer = reKeyer(React);

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
        this.getUpdateSate = this.getUpdateSate.bind(this);
        this.handleWheel = this.handleWheel.bind(this);
    }

    getUpdateSate(chan, index) {
        return () => this.setState(prevState => {
            // if (chan === 'rx') {
            //     prevState.sock.send(JSON.stringify({type: 'rx', val: prevState.rxFreq[index]}));
            //     prevState.sock.send(JSON.stringify({type: 'tx', val: prevState.txFreq[index]}));
            // }
            return {
                [chan]: index
            };
        });
    }

    handleWheel (chan) {
        return step => evnt => {
            console.log(evnt.deltaY);
            const delta = (evnt.deltaY > 0) ? -step : step;
            this.setState(prevState => {
                const idx = prevState['rx'];
                const val = prevState[chan + 'Freq'][idx];
                const newVal = val + delta;
                // prevState.sock.send(JSON.stringify({type: chan, val: newVal}));
                prevState[chan + 'Freq'][idx] = newVal;
                return prevState;
            });
        };
    }

    componentDidMount () {
        const water = this.refs.water;
        const gpio = getGpio(usb);
        // const pluto = getPluto(iio);
        // console.log(pluto);
        // const ctx = water.getContext('2d');
        // const host = window.location.hostname;
        // const port = window.location.port;
        // const sock = new WebSocket('ws://' + host + ':' + port, 'p1');
        // sock.binaryType = 'arraybuffer';
        // this.setState(() => ({sock: sock}));
        // sock.onopen = () => {};
        // sock.onmessage = event => {
        //     // spectr.scroll.left(water);
        //     // spectr.rainbow.right(water)(new Uint8Array(event.data));
        //     spectr.scroll.down(water);
        //     spectr.rainbow.top(water)(new Uint8Array(event.data));
        //     // const msg = JSON.parse(event.data);
        //     // this.setState(() => msg);
        // };
        // keyer(550, 10, 'x', 'z', this.getUpdateSate, sock);
    }

    render () {
        const state = this.state;
        return $('div', {},
            $(Pluto, {
                rxFreq: state.rxFreq[state.rx],
                pluto: state.pluto
            }),
            $(Gpio, {}),
            $(Keyer, {}),
            $('div', {},
                $('span', {}, '\u2647'),
                [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(idx => $('button', {
                    key: idx,
                    onClick: this.getUpdateSate('rx', idx),
                    className: (state.rx === idx) ? 'on' : 'off'
                }, Math.round(state.rxFreq[idx] / 1e6))),
                $('span', {}, ' '),
                $('button', {className: state.dash ? 'on' : 'off'}, '\u2212'),
                $('button', {className: state.dit  ? 'on' : 'off'}, '\u22C5'),
                $('button', {className: state.carr ? 'on' : 'off'}, 'CR'),
                $('button', {className: state.ptt  ? 'on' : 'off'}, 'PTT')
            ),
            $('div', {},
                $(RxFreq, {
                    className: 'rx',
                    val: state.rxFreq[state.rx],
                    handleWheel: this.handleWheel('rx')
                }),
                $('span', {}, '\u2606'),
                $(TxFreq, {
                    className: 'tx',
                    val: state.txFreq[state.rx],
                    handleWheel: this.handleWheel('tx')
                })
            ),
            $('canvas', {
                ref: 'water',
                height: 256,
                width: 1024,
                style: {backgroundColor: '#000'}
            })
        );
    }
}

ReactDOM.render(
    $(App, {
        rx: 0,
        tx: 0,
        rxFreq: [435140000, 144000000, 144000000, 432000000, 1296000000, 0, 0, 0, 0, 0],
        txFreq: [435140000, 144000000, 144000000, 432000000, 1296000000, 0, 0, 0, 0, 0],
        dit: false,
        dash: false,
        ptt: false,
        carr: false
    }),
    document.getElementById('root')
);

/* eslint-env browser */
