'use strict';

// simple, flat color box on grid

module.exports = React => {
    const $ = React.createElement;
    return function Box (p) {
        return $('rect', {
            width: p.width,
            height: p.height,
            style: {fill: p.fill || 'none', stroke: p.stroke || 'none'}
        });
    };
};
