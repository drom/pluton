'use strict';

// const iio = require('libiio');
// const getPluto = require('../lib/get-pluto');

const genKeyer = function (toneFreq, toneRate, ditKey, dashKey, getUpdateSate, sock) {
    const context = new AudioContext;

    let dit = false;
    let dash = false;
    let wasDit = false;
    let wasDash = false;
    let carr = false;
    let timer;

    const u = {
        dit: {
            down: getUpdateSate('dit', true),
            up: getUpdateSate('dit', false)
        },
        dash: {
            down: getUpdateSate('dash', true),
            up: getUpdateSate('dash', false)
        },
        ptt: {
            down: getUpdateSate('ptt', true),
            up: getUpdateSate('ptt', false)
        },
        carr: {
            down: getUpdateSate('carr', true),
            up: getUpdateSate('carr', false)
        }
    };

    const fsm = () => {
        if (timer === undefined) {
            if (wasDit || wasDash) {
                var osc = context.createOscillator();
                osc.frequency.value = toneFreq;
                osc.connect(context.destination);
                console.log('start ' + (wasDit ? '.' : '-'));
                const timeOn = (wasDit ? 1000 : 3000) / toneRate;
                const timeOff = 1000 / toneRate;
                osc.start();
                // sock.send(JSON.stringify({type: 'cw', on: timeOn, off: timeOff}));
                timer = setTimeout(() => {
                    osc.stop();
                    timer = setTimeout(() => {
                        timer = undefined;
                        wasDit |= dit;
                        wasDash |= dash;
                        fsm();
                    }, timeOff);
                }, timeOn);
                if (wasDash) {
                    wasDash = false;
                } else {
                    wasDit = false;
                }
            }
        }
    };

    document.addEventListener('keydown', event => {
        console.log('down ' + event.key);
        switch (event.key) {
        case ditKey: dit = true; wasDit = true; u.dit.down(); break;
        case dashKey: dash = true; wasDash = true; u.dash.down(); break;
        case 'm': u.ptt.down(); break;
        case 'c':
            u.carr.down();
            // sock.send(JSON.stringify({type: 'carr', val: 'on'}));
            break;
        }
        fsm();
    });

    document.addEventListener('keyup', event => {
        console.log('up ' + event.key);
        switch (event.key) {
        case ditKey: dit = false; u.dit.up(); break;
        case dashKey: dash = false; u.dash.up(); wasDash = false; break;
        case 'm': u.ptt.up(); break;
        case 'c':
            u.carr.up();
            // sock.send(JSON.stringify({type: 'carr', val: 'off'}));
            break;
        }
        fsm();
    });
};

module.exports = React => {
    // const $ = React.createElement;
    return class Keyer extends React.PureComponent {

        constructor(props) {
            super(props);
            this.keyer = genKeyer(550, 10, 'x', 'z', () => () => {});
            // this.pluto = getPluto(iio);
        }

        render () {
            return null;
        }

    };
};
/* eslint-env browser */
