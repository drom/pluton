'use strict';

const iio = require('libiio');
const getPluto = require('../lib/get-pluto');

module.exports = React => {
    const $ = React.createElement;
    return class Pluto extends React.PureComponent {

        constructor(props) {
            super(props);
            this.pluto = getPluto(iio);
        }

        render () {
            const freq = this.props.rxFreq;
            const pluto = this.pluto;
            if (pluto === undefined) {
                return $('div', {}, 'please connect Pluto board');
            } else {
                pluto.phy.chan.output.altvoltage1.attr.frequency.setString(freq);
                return null; // $('div', {}, freq);
            }
        }

    };
};
