'use strict';

exports.genGetData = length => {
    let arr = new Uint8Array(length);
    return () => {
        for (let i = 0; i < length; i++) {
            arr[i] = Math.random() * (1 << 8);
        }
        return arr;
    };
};

exports.rainbow = {
    right: cnvs => arr => {
        const offset = cnvs.width - 1;
        const cxt = cnvs.getContext('2d');
        const len = arr.length;
        for (let i = 0; i < len; i++) {
            cxt.fillStyle = 'hsl(' + (256 - arr[i]) + ', 100%, 50%)';
            cxt.fillRect(offset, i, 1, 1);
        }
    },
    top: cnvs => arr => {
        const cxt = cnvs.getContext('2d');
        const len = arr.length;
        for (let i = 0; i < len; i++) {
            cxt.fillStyle = 'hsl(' + (256 - arr[i]) + ', 100%, 50%)';
            cxt.fillRect(i, 0, 1, 1);
        }
    }
};

exports.scroll = {
    left: cnvs => {
        const w = cnvs.width - 1;
        const h = cnvs.height;
        cnvs.getContext('2d').drawImage(cnvs, 1, 0, w, h, 0, 0, w, h);
    },
    down: cnvs => {
        const w = cnvs.width;
        const h = cnvs.height - 1;
        cnvs.getContext('2d').drawImage(cnvs, 0, 0, w, h, 0, 1, w, h);
    }
};

exports.RL = (h, w) => {
    const cnvs = document.createElement('canvas');
    cnvs.height = h;
    cnvs.width = w;
    cnvs.style = 'background-color: #000;';
    const getData = genGetData(h);
    setInterval(function run () {
        scroll(cnvs);
        rainbow(cnvs)(getData());
    }, 40);
    return cnvs;
};
