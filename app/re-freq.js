'use strict';

module.exports = function (React) {
    const $ = React.createElement;
    return function render (props) {
        const val = props.val.toString(10);
        const d = Array(10)
            .fill(' ')
            .map((_, i) => val.charAt(val.length - i - 1) || '\u2007');
        return $('span', {className: props.className + ' freq'},
            $('span', {className: 'frontend'}, props.className + '1'),
            $('span', {onWheel: props.handleWheel(1e9), className: 'digit'}, d[9]),
            $('span', {}, (props.val >= 1e9) ? '.' : '\u2008'),
            $('span', {onWheel: props.handleWheel(1e8), className: 'digit'}, d[8]),
            $('span', {onWheel: props.handleWheel(1e7), className: 'digit'}, d[7]),
            $('span', {onWheel: props.handleWheel(1e6), className: 'digit'}, d[6]),
            $('span', {}, '.'),
            $('span', {onWheel: props.handleWheel(1e5), className: 'digit'}, d[5]),
            $('span', {onWheel: props.handleWheel(1e4), className: 'digit'}, d[4]),
            $('span', {onWheel: props.handleWheel(1e3), className: 'digit'}, d[3]),
            $('span', {}, '.'),
            $('span', {onWheel: props.handleWheel(1e2), className: 'digit'}, d[2]),
            $('span', {className: 'fixedigit'}, d[1]),
            $('span', {className: 'fixedigit'}, d[0]),
        );
    };
};
