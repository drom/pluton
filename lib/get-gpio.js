'use strict';

module.exports = usb => {
    let gpio;
    usb.getDeviceList().some((dev, i) => {
        if (dev.deviceDescriptor.idVendor === 0x0403 &&
            dev.deviceDescriptor.idProduct === 0x6014) {
            gpio = dev;
            dev.open();
            const iSerialNumber = dev.deviceDescriptor.iSerialNumber;
            dev.getStringDescriptor(iSerialNumber, (err, buf) => {
                console.log(i, buf);
                const if0 = dev.interfaces[0];
                if (if0.isKernelDriverActive()) {
                    if0.detachKernelDriver();
                    console.log('detached');
                }
                dev.close();
            });
        }
    });
    if (gpio !== undefined) {
        console.log('found: FT232H control board');
    } else {
        // throw new Error('plese connect FT232H control board');
    }
    return gpio;
};
