'use strict';

const usb = require('usb');
const getGpio = require('../lib/get-gpio');

module.exports = React => {
    const $ = React.createElement;
    return class Pluto extends React.PureComponent {

        constructor(props) {
            super(props);
            this.gpio = getGpio(usb);
        }

        render () {
            // const freq = this.props.rxFreq;
            const gpio = this.gpio;
            if (gpio === undefined) {
                return $('div', {}, 'please connect FT232H board');
            } else {
                // pluto.phy.chan.output.altvoltage1.attr.frequency.setString(freq);
                return null; // $('div', {}, freq);
            }
        }

    };
};
