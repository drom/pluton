#!/usr/bin/env node

'use strict';

const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const portfinder = require('portfinder');
const ip = require('ip');
const usb = require('usb');
const iio = require('libiio');
const opn = require('opn');

const getGpio = () => {
    let gpio;
    usb.getDeviceList().some((dev, i) => {
        if (dev.deviceDescriptor.idVendor === 0x0403 &&
            dev.deviceDescriptor.idProduct === 0x6014) {
            gpio = dev;
            dev.open();
            const iSerialNumber = dev.deviceDescriptor.iSerialNumber;
            dev.getStringDescriptor(iSerialNumber, (err, buf) => {
                console.log(i, buf);
                const if0 = dev.interfaces[0];
                if (if0.isKernelDriverActive()) {
                    if0.detachKernelDriver();
                    console.log('detached');
                }
                dev.close();
            });
        }
    });
    if (gpio !== undefined) {
        console.log('found: FT232H control board');
    } else {
        throw new Error('plese connect FT232H control board');
    }
    return gpio;
};

const getPluto = () => {
    const contexts = iio.discover('usb');

    if (contexts.length > 0) {
        console.log('found: Pluto SDR board');
    } else {
        throw new Error('please connect Pluto SDR board');
    }

    const pluto0 = contexts[0].devices;

    const dev = {
        tx:  pluto0['cf-ad9361-dds-core-lpc'],
        rx:  pluto0['cf-ad9361-lpc'],
        phy: pluto0['ad9361-phy']
    };

    dev.phy.chan.output.voltage0.attr.hardwaregain.setString(0);

    dev.tx.chan.output.voltage0.attr.sampling_frequency.setString(262500);
    dev.tx.chan.output.voltage1.attr.sampling_frequency.setString(262500);

    dev.tx.chan.output.altvoltage0.attr.sampling_frequency.setString(2090000); // TX1_I_F1
    dev.tx.chan.output.altvoltage1.attr.sampling_frequency.setString(2090000); // TX1_I_F2
    dev.tx.chan.output.altvoltage2.attr.sampling_frequency.setString(2090000); // TX1_Q_F1
    dev.tx.chan.output.altvoltage3.attr.sampling_frequency.setString(2090000); // TX1_Q_F2

    dev.tx.chan.output.altvoltage0.attr.frequency.setString(0); // TX1_I_F1
    dev.tx.chan.output.altvoltage1.attr.frequency.setString(0); // TX1_I_F2
    dev.tx.chan.output.altvoltage2.attr.frequency.setString(0); // TX1_Q_F1
    dev.tx.chan.output.altvoltage3.attr.frequency.setString(0); // TX1_Q_F2

    dev.tx.chan.output.altvoltage0.attr.scale.setString(0.5); // TX1_I_F1
    dev.tx.chan.output.altvoltage1.attr.scale.setString(0.5); // TX1_I_F2
    dev.tx.chan.output.altvoltage2.attr.scale.setString(0.5); // TX1_Q_F1
    dev.tx.chan.output.altvoltage3.attr.scale.setString(0.5); // TX1_Q_F2

    dev.phy.chan.output.voltage2.attr.sampling_frequency.setString(2090000);
    dev.phy.chan.output.voltage3.attr.sampling_frequency.setString(2090000);

    return dev;
};

const genGetData = length => {
    let arr = new Uint8Array(length);
    return () => {
        for (let i = 0; i < length; i++) {
            arr[i] = Math.random() * (1 << 8);
        }
        return arr;
    };
};

const getServer = (gpio, dev) => {
    const app = express();
    app.use(express.static('public'));
    const server = http.createServer(app);
    const wss = new WebSocket.Server({server});
    const getData = genGetData(1024);

    wss.on('connection', (ws, req) => {
        console.log('connected');

        setInterval(function run () {
            ws.send(getData());
        }, 40);

        ws.on('message', msg => {
            const message = JSON.parse(msg);
            console.log(message);
            if (message.type === 'rx') {
                dev.phy.chan.output.altvoltage1.attr.frequency.setString(message.val); // TX LO
            } else
            if (message.type === 'cw') {
                dev.phy.chan.output.altvoltage1.attr.powerdown.setString(0);
                setTimeout(() => {
                    dev.phy.chan.output.altvoltage1.attr.powerdown.setString(1);
                }, message.on);
            } else
            if (message.type === 'carr') {
                if (message.val === 'on') {
                    dev.phy.chan.output.altvoltage1.attr.powerdown.setString(0);
                } else {
                    dev.phy.chan.output.altvoltage1.attr.powerdown.setString(1);
                }
            }
        });
    });

    portfinder.getPort((err, port) => {
        if (err) { throw err; }
        server.listen(port, () => {
            const addr = 'http://' + ip.address() + ':' + server.address().port + '/';
            console.log('(Ctrl + Click) on:\n' + addr);
            opn(addr, {app: ['chromium', '--incognito']});
        });
    });

    return server;
};

(() => {
    const gpio = getGpio();
    const pluto = getPluto();
    const server = getServer(gpio, pluto);
})();
