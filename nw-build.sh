#!/usr/bin/env bash
npm i --build-from-source
cp assets/logo.png src/assets/
rm -rf src/node_modules
mkdir src/node_modules
mkdir src/node_modules/libiio/
mkdir src/node_modules/usb/
cp -r ./node_modules/libiio/* src/node_modules/libiio/
cp -r ./node_modules/usb/* src/node_modules/usb/
./node_modules/.bin/browserify -u usb -u libiio app/nw-app.js -o src/app.js
wget https://raw.githubusercontent.com/drom/simple-nwjs-app/master/build.sh -O build.sh
bash ./build.sh -p sdk- -n pluton --version v0.1.0
